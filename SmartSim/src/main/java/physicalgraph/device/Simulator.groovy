package physicalgraph.device

class Simulator {
	static main(args){
		def sim = new Simulator()
		sim.executeScript2()
	}

	def executeScript2(){
		GroovyShell shell = new GroovyShell()
		shell.getContext().setVariable("definition", {println "calling definition"})
		shell.getContext().setVariable("preferences", {println "calling preferences"})
		shell.getContext().setVariable("unsubscribe", {println "calling unsubscribe"})
		shell.getContext().setVariable("unschedule", {println "calling unschedule"})
		shell.getContext().setVariable("schedule", {println "calling schedule"})
		
				
		shell.getContext().setVariable("log", new STLog())
		shell.getContext().setVariable("settings", [:])
		shell.getContext().setVariable("state", [:])
		
		def scriptText = new File("C:\\Users\\Louis\\workspaces\\home\\SmartSim\\src\\main\\java\\TestScript1.groovy").text
		def script = shell.parse(scriptText)
		script.getMetaClass().methods.each{println it}
		//script.invokeMethod("scheduledActionsHandler", null)
		script.invokeMethod("installed", null)

	} 
	def executeScript(){
		GroovyShell shell = new GroovyShell()
		shell.getContext().setVariable("definition", {println "calling definition"})
		shell.getContext().setVariable("preferences", {println "calling preferences"})
		shell.getContext().setVariable("unsubscribe", {println "calling unsubscribe"})
		shell.getContext().setVariable("unschedule", {println "calling unschedule"})
		shell.getContext().setVariable("schedule", {println "calling schedule"})
		
				
		shell.getContext().setVariable("log", new STLog())
		shell.getContext().setVariable("settings", [:])
		shell.getContext().setVariable("state", [:])
		
		def scriptText = new File("C:\\Users\\Louis\\workspaces\\home\\SmartSim\\src\\main\\java\\script1.groovy").text
		def script = shell.parse(scriptText)
		script.getMetaClass().methods.each{println it}
		//script.invokeMethod("scheduledActionsHandler", null)
		script.invokeMethod("installed", null)
	}
}

class STLog{
	def methodMissing(String name, args){
		println " ${new Date().format('HH:mm:ss')} $name  $args"
	}
}